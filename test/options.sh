#!/bin/bash -e
mkdir test1
mkdir test2
../fusecrush test1 test2 -o cache_skipped,detach
fusermount -u test2
../fusecrush test1 -o cache_skipped,detach
fusermount -u test1
../fusecrush -o cache_skipped,detach test1 test2
fusermount -u test2
../fusecrush -o cache_skipped,detach test1
fusermount -u test1
rmdir test1
rmdir test2
